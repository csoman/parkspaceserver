package com.example.parkspace.server;

/*
 * Copyright 2009 Red Hat, Inc.
 * 
 * Red Hat licenses this file to you under the Apache License, version 2.0 (the
 * "License"); you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

import static org.jboss.netty.handler.codec.http.HttpHeaders.isKeepAlive;
import static org.jboss.netty.handler.codec.http.HttpHeaders.Names.CONTENT_LENGTH;
import static org.jboss.netty.handler.codec.http.HttpHeaders.Names.CONTENT_TYPE;
import static org.jboss.netty.handler.codec.http.HttpHeaders.Names.COOKIE;
import static org.jboss.netty.handler.codec.http.HttpHeaders.Names.SET_COOKIE;
import static org.jboss.netty.handler.codec.http.HttpResponseStatus.OK;
import static org.jboss.netty.handler.codec.http.HttpVersion.HTTP_1_1;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.ChannelFuture;
import org.jboss.netty.channel.ChannelFutureListener;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ExceptionEvent;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.channel.SimpleChannelUpstreamHandler;
import org.jboss.netty.handler.codec.http.Cookie;
import org.jboss.netty.handler.codec.http.CookieDecoder;
import org.jboss.netty.handler.codec.http.CookieEncoder;
import org.jboss.netty.handler.codec.http.DefaultHttpResponse;
import org.jboss.netty.handler.codec.http.HttpChunk;
import org.jboss.netty.handler.codec.http.HttpChunkTrailer;
import org.jboss.netty.handler.codec.http.HttpRequest;
import org.jboss.netty.handler.codec.http.HttpResponse;
import org.jboss.netty.handler.codec.http.QueryStringDecoder;
import org.jboss.netty.util.CharsetUtil;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.example.parkspace.dataEngine.GeoLocation;
import com.example.parkspace.dataEngine.IParkingData;
import com.example.parkspace.dataEngine.InMemoryParkingData;
import com.example.parkspace.dataEngine.ReservationStatus;

/**
 * @author <a href="http://www.jboss.org/netty/">The Netty Project</a>
 * @author Andy Taylor (andy.taylor@jboss.org)
 * @author <a href="http://gleamynode.net/">Trustin Lee</a>
 * 
 * @version $Rev: 2288 $, $Date: 2010-05-27 21:40:50 +0900 (Thu, 27 May 2010) $
 */
public class HttpRequestHandler extends SimpleChannelUpstreamHandler {

	private HttpRequest request;
	private boolean readingChunks;
	/** Buffer that stores the response content */
	private final StringBuilder buf = new StringBuilder();
	private ChannelBuffer responseContent;
	private final static String UID = "uid";
	private final static String USER_INFO = "user_info";
	private final static String LATITUDE = "latitude";
	private final static String LONGITUDE = "longitude";
	private static final Object SELLER_UID = "seller";
	private IParkingData dataEngine;

	private static enum OP_TYPE {
		REGISTER,
		REGISTER_PARKING_SLOT,
		GET_PARKING_SLOTS,
		RESERVE_REQUEST,
		RESERVE_ACCEPT,
		RESERVE_CLOSE,
		POLL_RESERVE_REQUEST,
		ERROR
	};

	public HttpRequestHandler(IParkingData dataEngine) {
		this.dataEngine = dataEngine;
	}

	private OP_TYPE getOperationType(String path) {
		if(path.equals("/register")) {
			return OP_TYPE.REGISTER;
		} else if (path.equals("/register_parking_slot")) {
			return OP_TYPE.REGISTER_PARKING_SLOT;
		} else if (path.equals("/reserve_request")) {
			return OP_TYPE.RESERVE_REQUEST;
		} else if (path.equals("/reserve_accept")) {
			return OP_TYPE.RESERVE_ACCEPT;
		} else if (path.equals("/reserve_close")) {
			return OP_TYPE.RESERVE_CLOSE;
		} else if (path.equals("/get_parking_slots")) {
			return OP_TYPE.GET_PARKING_SLOTS;
		} else if (path.equals("/poll_reserve_request")) {
			return OP_TYPE.POLL_RESERVE_REQUEST;
		}

		return OP_TYPE.ERROR;
	}

	@Override
	public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) throws Exception {
		String requestUid = "";

		if(!readingChunks) {
			HttpRequest request = this.request = (HttpRequest) e.getMessage();
			QueryStringDecoder queryStringDecoder = new QueryStringDecoder(request.getUri());

			// Decode the operation type
			OP_TYPE operation = getOperationType(queryStringDecoder.getPath());

			Map<String, List<String>> params = queryStringDecoder.getParameters();
			if(params != null && params.containsKey(UID)) {
				requestUid = params.get(UID).get(0);
			} else {
				System.err.println("UID missing. Critical error");
				this.responseContent = ChannelBuffers.copiedBuffer("UID missing. Critical error".getBytes());
				writeResponse(e);
				return;
				// TODO: Return the right error code here
			}

			// System.out.println("Working with store: " + storeName);

			if(request.isChunked()) {
				readingChunks = true;
			} else {

//				ChannelBuffer content = request.getContent();
//				if(!content.readable()) {
//					System.err.println("Contents not readable");
//					this.responseContent = ChannelBuffers.copiedBuffer("Contents not readable".getBytes());
//				}

				// TODO: Check for correct number of parameters and Decoding

				System.out.println ("Doing Operation : " + operation);
				
				switch(operation) {
				case REGISTER:
					String userInfo = "";
					
					// This is optional
					if (params.containsKey(USER_INFO)) {
						userInfo = params.get(USER_INFO).get(0);
					}
					this.dataEngine.registerUser(requestUid, userInfo);
					System.out.println("New user : " + requestUid + " successfully registered.");
					break;
					
				case REGISTER_PARKING_SLOT:
					System.out.println("Inside register parking slot");
					if (!params.containsKey(LATITUDE) || !params.containsKey(LONGITUDE)) {
						System.err.println("Incorrect params: Requires UID, LATITUDE AND LONGITUDE");
						this.responseContent = ChannelBuffers.copiedBuffer("Incorrect params: Requires UID, LATITUDE AND LONGITUDE".getBytes());
						break;
					}
					double latitude = Double.parseDouble(params.get(LATITUDE).get(0));
					double longitude = Double.parseDouble(params.get(LONGITUDE).get(0));
					this.dataEngine.registerFreeParking(requestUid, new GeoLocation(latitude, longitude));
					System.out.println("New location for seller : " + requestUid + " successfully registered at location : (" + latitude + ", " + longitude + ")" );
					break;
				case GET_PARKING_SLOTS:
//					if (!params.containsKey(LATITUDE) || !params.containsKey(LONGITUDE)) {
//						System.err.println("Incorrect params: Requires UID, LATITUDE AND LONGITUDE");
//						this.responseContent = ChannelBuffers.copiedBuffer("Incorrect params: Requires UID, LATITUDE AND LONGITUDE".getBytes());
//						break;
//					}
//					double currentLatitude = Double.parseDouble(params.get(LATITUDE).get(0));
//					double currentLongitude = Double.parseDouble(params.get(LONGITUDE).get(0));
					Map <String, GeoLocation> sellerLocationMap = this.dataEngine.getAllSlots();
					JSONArray resultList = new JSONArray();
					for (Entry<String, GeoLocation> sellerLocationEntry : sellerLocationMap.entrySet()) {
						GeoLocation parkingLoc = sellerLocationEntry.getValue();
						JSONObject parkingEntry = new JSONObject();
						parkingEntry.put("uid", sellerLocationEntry.getKey());
						parkingEntry.put("latitude", parkingLoc.getLatitude());
						parkingEntry.put("longitude", parkingLoc.getLongitude());
						resultList.add(parkingEntry);
					}
					System.out.println("Returning response : " + resultList.toJSONString());
					this.responseContent = ChannelBuffers.copiedBuffer(resultList.toJSONString().getBytes());
					
					// TODO : Create a JSON response
					break;
				case RESERVE_REQUEST:
					if (!params.containsKey(SELLER_UID)) {
						System.err.println("Incorrect params: Requires UID and SELLER UID");
						this.responseContent = ChannelBuffers.copiedBuffer("Incorrect params: Requires UID and SELLER UID".getBytes());
						break;
					}
					String sellerUid = params.get(SELLER_UID).get(0);
					this.dataEngine.newReservation(requestUid, sellerUid);
					System.out.println("New reservation made by : " + requestUid + " for seller :." + sellerUid);
					break;
				case RESERVE_ACCEPT:
					if (!params.containsKey(SELLER_UID)) {
						System.err.println("Incorrect params: Requires UID and SELLER UID");
						this.responseContent = ChannelBuffers.copiedBuffer("Incorrect params: Requires UID and SELLER UID".getBytes());
						break;
					}
					String acceptSellerUid = params.get(SELLER_UID).get(0);
					this.dataEngine.updateReservation(requestUid, acceptSellerUid, ReservationStatus.ACCEPTED);
					System.out.println("Reservation made by : " + requestUid + " for seller :." + acceptSellerUid + " successfully accepted.");
					break;
				case RESERVE_CLOSE:
					if (!params.containsKey(SELLER_UID)) {
						System.err.println("Incorrect params: Requires UID and SELLER UID");
						this.responseContent = ChannelBuffers.copiedBuffer("Incorrect params: Requires UID and SELLER UID".getBytes());
						break;
					}
					String closedSellerUid = params.get(SELLER_UID).get(0);
					this.dataEngine.updateReservation(requestUid, closedSellerUid, ReservationStatus.CLOSED);
					System.out.println("Reservation made by : " + requestUid + " for seller :." + closedSellerUid + " successfully closed.");
					break;
				case POLL_RESERVE_REQUEST:
					String potentialBuyer = this.dataEngine.pollReservation(requestUid);
					if (potentialBuyer == null) {
						potentialBuyer = "***NULL***";
					}
					System.out.println("Returning poll response for uid : " + requestUid + " as : " + potentialBuyer);
					this.responseContent = ChannelBuffers.copiedBuffer(potentialBuyer.getBytes());
					break;
				default:
					System.err.println("Illegal operation.");
					this.responseContent = ChannelBuffers.copiedBuffer("Illegal operation.".getBytes());
					return;
				}

				writeResponse(e);
			}
		} else {
			HttpChunk chunk = (HttpChunk) e.getMessage();
			if(chunk.isLast()) {
				readingChunks = false;
				buf.append("END OF CONTENT\r\n");

				HttpChunkTrailer trailer = (HttpChunkTrailer) chunk;
				if(!trailer.getHeaderNames().isEmpty()) {
					buf.append("\r\n");
					for(String name: trailer.getHeaderNames()) {
						for(String value: trailer.getHeaders(name)) {
							buf.append("TRAILING HEADER: " + name + " = " + value + "\r\n");
						}
					}
					buf.append("\r\n");
				}

				writeResponse(e);
			} else {
				buf.append("CHUNK: " + chunk.getContent().toString(CharsetUtil.UTF_8) + "\r\n");
			}

		}
	}

	private void writeResponse(MessageEvent e) {
		// Decide whether to close the connection or not.
		boolean keepAlive = isKeepAlive(request);

		// Build the response object.
		HttpResponse response = new DefaultHttpResponse(HTTP_1_1, OK);
		response.setContent(this.responseContent);
		response.setHeader(CONTENT_TYPE, "text/plain; charset=UTF-8");
		//        response.setHeader(CONTENT_TYPE, "application/pdf");

		if(keepAlive) {
			// Add 'Content-Length' header only for a keep-alive connection.
			response.setHeader(CONTENT_LENGTH, response.getContent().readableBytes());
		}

		// Encode the cookie.
		String cookieString = request.getHeader(COOKIE);
		if(cookieString != null) {
			CookieDecoder cookieDecoder = new CookieDecoder();
			Set<Cookie> cookies = cookieDecoder.decode(cookieString);
			if(!cookies.isEmpty()) {
				// Reset the cookies if necessary.
				CookieEncoder cookieEncoder = new CookieEncoder(true);
				for(Cookie cookie: cookies) {
					cookieEncoder.addCookie(cookie);
				}
				response.addHeader(SET_COOKIE, cookieEncoder.encode());
			}
		}

		// Write the response.
		ChannelFuture future = e.getChannel().write(response);

		// Close the non-keep-alive connection after the write operation is
		// done.
		if(!keepAlive) {
			future.addListener(ChannelFutureListener.CLOSE);
		}
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e) throws Exception {
		e.getCause().printStackTrace();
		e.getChannel().close();
	}
}
