package com.example.parkspace.dataEngine;

import java.util.List;
import java.util.Map;

public interface IParkingData {
	
	public void registerUser (String uid, String userInfo);
	public String getUserInfo (String uid);
	public void registerFreeParking (String uid, GeoLocation location);
	public List<GeoLocation> getParkingSlots (GeoLocation currentLocation);
	public Map<String, GeoLocation> getAllSlots ();
	public void newReservation (String buyer, String seller);
	public void updateReservation (String buyer, String seller, ReservationStatus status);
	public String pollReservation (String seller);
}
