package com.example.parkspace.dataEngine;

public enum ReservationStatus {
	NEW,
	ACCEPTED,
	CLOSED
}
