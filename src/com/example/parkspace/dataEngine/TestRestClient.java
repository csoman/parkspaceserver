package com.example.parkspace.dataEngine;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

public class TestRestClient {

	private static final String serviceURL = "http://ec2-204-236-160-57.us-west-1.compute.amazonaws.com:8080";
//	private static final String serviceURL = "http://localhost:8080";

	private static class SellerLocation {
		private String sellerUid;
		private double latitude;
		private double longitude;

		public SellerLocation (String uid, double latitude, double longitude) {
			setSellerUid(uid);
			setLatitude(latitude);
			setLongitude(longitude);
		}

		public String getSellerUid() {
			return sellerUid;
		}
		public void setSellerUid(String sellerUid) {
			this.sellerUid = sellerUid;
		}
		public double getLatitude() {
			return latitude;
		}
		public void setLatitude(double latitude) {
			this.latitude = latitude;
		}
		public double getLongitude() {
			return longitude;
		}
		public void setLongitude(double longitude) {
			this.longitude = longitude;
		}

		@Override
		public String toString() {
			return this.sellerUid + " at (" + this.latitude + ", " + this.longitude + ")";
		}

	}

	public static void registerUser (String uid) {
		try {
			URL url = new URL(serviceURL + "/register?uid=" + uid);
			HttpURLConnection conn =
					(HttpURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setDoOutput(true);
			conn.setDoInput(true);

			if (conn.getResponseCode() != 200) {
				System.out.println(conn.getResponseCode());
				System.err.println("Illegal response : " + conn.getResponseMessage());               
			}
		}catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void registerParkingSlot (SellerLocation sellerLoc) {
		try {
			URL url = new URL(serviceURL + "/register_parking_slot?uid=" + sellerLoc.getSellerUid() + "&latitude=" + sellerLoc.getLatitude() + "&longitude=" + sellerLoc.getLongitude());
			HttpURLConnection conn =
					(HttpURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setDoOutput(true);
			conn.setDoInput(true);

			if (conn.getResponseCode() != 200) {
				System.out.println(conn.getResponseCode());
				System.err.println("Illegal response : " + conn.getResponseMessage());               
			}
		}catch (Exception e) {
			e.printStackTrace();
		}		
	}

	public static void getParkingLocations () {
		try {
			URL url = new URL(serviceURL + "/get_parking_slots?uid=chinmay");
			//			URL url = new URL("http://localhost:8080/get_parking_slots?uid=chinmay");
			HttpURLConnection conn =
					(HttpURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setDoOutput(true);
			conn.setDoInput(true);

			if (conn.getResponseCode() != 200) {
				System.out.println(conn.getResponseCode());
				System.err.println("Illegal response : " + conn.getResponseMessage());               
			} else {
				BufferedReader msgReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				StringBuilder sb = new StringBuilder();
				String line;
				while ((line = msgReader.readLine()) != null) {
					sb.append(line);
				}
				msgReader.close();
				System.out.println(sb);

				// Parse the JSON
				JSONArray locationArray = (JSONArray)JSONValue.parse(sb.toString());
				for (int i = 0; i < locationArray.size(); i++) {
					JSONObject sellerLoc = (JSONObject) locationArray.get(i);
					String uid = (String)sellerLoc.get("uid");
					double latitude = (Double) sellerLoc.get("latitude");
					double longitude = (Double) sellerLoc.get("longitude");
					SellerLocation locationOverlay = new SellerLocation(uid, latitude, longitude);
					System.out.println(locationOverlay);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	
	public static String pollForBuyer (String uid) {
		try {
			URL url = new URL(serviceURL + "/poll_reserve_request?uid=" + uid);
			HttpURLConnection conn =
					(HttpURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setDoOutput(true);
			conn.setDoInput(true);

			if (conn.getResponseCode() != 200) {
				System.out.println(conn.getResponseCode());
				System.err.println("Illegal response : " + conn.getResponseMessage());               
			} else {
				BufferedReader msgReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				StringBuilder sb = new StringBuilder();
				String line;
				while ((line = msgReader.readLine()) != null) {
					sb.append(line);
				}
				msgReader.close();
				
				if (!sb.toString().equals("***NULL***")) {
					return sb.toString();
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	
	public static void reservationRequest (String buyer, String seller) {
		try {
			URL url = new URL(serviceURL + "/reserve_request?uid=" + buyer + "&seller=" + seller);
			HttpURLConnection conn =
					(HttpURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setDoOutput(true);
			conn.setDoInput(true);

			if (conn.getResponseCode() != 200) {
				System.out.println(conn.getResponseCode());
				System.err.println("Illegal response : " + conn.getResponseMessage());               
			}
		}catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	
	public static String timedPollForBuyer(String uid, int timeInSeconds) {
		long currentTime = System.currentTimeMillis();
		long targetTime = currentTime + (timeInSeconds * 1000);
		String response = null;
		
		while (currentTime < targetTime) {
			response = pollForBuyer(uid);
			if (response == null) {
				System.out.println("Did not get anything ... sleeping for 500 milliseconds. ");
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				currentTime = System.currentTimeMillis();
			}
		}
		return response;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
//		registerUser("chinmay");
//		registerUser("Ben");
		SellerLocation loc1 = new SellerLocation("chinmay", 374291, -1221751);
		SellerLocation loc2 = new SellerLocation("Ben", 374271, -1221731);
		SellerLocation loc3 = new SellerLocation("angad", 374291, -1221951);
		SellerLocation loc4 = new SellerLocation("mihitha", 374271, -1221531);

		registerParkingSlot(loc1);
		registerParkingSlot(loc2);
		registerParkingSlot(loc3);
		registerParkingSlot(loc4);
		
		double startLoc = 37428000;
		double startLong = -122174000;
		for (int i = 0; i<50;i++) {
			registerParkingSlot(new SellerLocation("test-user" + i, startLoc, startLong));
			startLoc += (int) (Math.random() * 1000);
			startLong += (int) (Math.random() * 1000);
		}
		
//		getParkingLocations();
		
//		reservationRequest("chinmay", "Ben");
		
//		System.out.println("Polling for ben => potential buyers : " + pollForBuyer("Ben"));
//		System.out.println("Polling for chinmay => potential buyers : " + timedPollForBuyer("chinmay", 5));

	} 

}


