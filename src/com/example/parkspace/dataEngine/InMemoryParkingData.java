package com.example.parkspace.dataEngine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class InMemoryParkingData implements IParkingData {
	
	private class Reservation {
		private String buyer;
		private ReservationStatus status;
		
		public Reservation(String buyer, ReservationStatus status) {
			setBuyer(buyer);
			setStatus(status);
		}
		
		public String getBuyer() {
			return buyer;
		}
		public void setBuyer(String seller) {
			this.buyer = seller;
		}
		public ReservationStatus getStatus() {
			return status;
		}
		public void setStatus(ReservationStatus status) {
			this.status = status;
		}
	}
	
	private HashMap<String, String> userInfoMap = new HashMap<String, String>();
	private HashMap <String, GeoLocation> userLocationMap = new HashMap<String, GeoLocation>();
	private HashMap <String, Reservation> sellerReservationMap = new HashMap<String, Reservation>();

	@Override
	public void registerUser(String uid, String userInfo) {
		this.userInfoMap.put(uid, userInfo);
	}

	@Override
	public void registerFreeParking(String uid, GeoLocation location) {
		this.userLocationMap.put(uid, location);
	}

	@Override
	public List<GeoLocation> getParkingSlots(GeoLocation currentLocation) {
		List<GeoLocation> locationList = new ArrayList<GeoLocation>();
		for (Entry<String, GeoLocation> locationEntry : this.userLocationMap.entrySet()) {
			locationList.add(locationEntry.getValue());
		}
		return locationList;
	}

	@Override
	public void newReservation(String buyer, String seller) {
		this.sellerReservationMap.put(seller, new Reservation(buyer, ReservationStatus.NEW));
	}

	@Override
	public void updateReservation(String buyer, String seller,
			ReservationStatus status) {
		this.sellerReservationMap.put(seller, new Reservation(buyer, status));
	}

	@Override
	public String getUserInfo(String uid) {
		return this.userInfoMap.get(uid);
	}

	@Override
	public Map<String, GeoLocation> getAllSlots() {
		return this.userLocationMap;
	}

	@Override
	public String pollReservation(String seller) {
		if (this.sellerReservationMap.containsKey(seller)) {
			return this.sellerReservationMap.get(seller).getBuyer();
		}
		return null;
	}

}
